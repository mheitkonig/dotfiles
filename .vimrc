set ignorecase
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
set splitbelow
set splitright
syntax on
set shellcmdflag=-ic
set undofile
set undodir=~/.vim/undodir
"set hlsearch
"set cul

" -- Default theming stuff --
set background=dark
colo gruvbox


" -- Custom commands --
:command! Pd execute ':silent !'.'pdl paper >/dev/null &\!' | execute ':redraw!' 
:command! Pdv execute '!pdl paper'
map Ω :w <CR> :Pd <CR>
map å :noh <CR>



" -- Color lists (alt <digit>)
map ¡ :colo gruvbox<CR> :set bg=dark<CR>
map € :colo gruvbox<CR> :set bg=light<CR>
map £ :colo newsprint<CR>
map ¢ :colo print_bw<CR>
map ∞ :colo C64<CR>
map § :colo Monokai<CR>
map ¶ :colo last256<CR>
map • :colo nature<CR>
map ª :colo blazer<CR>
map º :colo nord<CR>
map – :colo solarized8_light_high<CR>
map ≠ :colo solarized8_dark_high<CR>

" -- Other shortcuts (shift+alt <digit>)
map ⁄ :set spell <CR>
map ™ :set nospell <CR>
map ‹ :CocEnable <CR>
map › :CocDisable <CR>
"map ﬁ
"map ﬂ
"map ‡
"map °
"map ·
"map —
"map ±

" -- Filetype options --
autocmd Filetype python,rust call ProgrammingOptions()
autocmd FileType latex,tex,md,markdown call SetWritingOptions()
autocmd FileType bib setlocal paste

" -- Programming options
function ProgrammingOptions()
	setlocal tabstop=4
	setlocal nospell
	setlocal number
endfunction

" -- LaTeX/Markdown options
function SetWritingOptions()
	setlocal spell
	setlocal tabstop=2
	setlocal textwidth=75
	setlocal colorcolumn=+2
	highlight ColorColumn ctermbg=0
endfunction

call plug#begin('~/.vim/vimplugs')
Plug '~/.fzf'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()

augroup CocGroup
  autocmd!
  autocmd BufNew,BufRead * execute "silent! CocDisable"
  autocmd BufNew,BufEnter *.c execute "silent! CocEnable"
  autocmd BufNew,BufEnter *.py execute "silent! CocEnable"
  "autocmd BufNew,BufEnter *.java execute "silent! CocEnable"
  autocmd BufNew,BufEnter *.rs execute "silent! CocEnable"
augroup end
