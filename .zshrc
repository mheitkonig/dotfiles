PROMPT='%F{yellow}%1~ %f'

autoload -U compinit promptinit
zstyle ':completion:*' menu select
compinit
_comp_options+=(globdots) # Show hidden files
promptinit
autoload -U colors && colors

setopt noflowcontrol
setopt correct

#bindkey -e

# History configuration
HISTFILE=~/.config/zsh/.zsh_history
HISTSIZE=50000
SAVEHIST=50000
setopt extendedglob
setopt appendhistory
setopt HIST_IGNORE_DUPS

